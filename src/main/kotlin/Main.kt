import java.lang.NumberFormatException

/**
 * Programa que recull una série de problemes que l'usuari ha de resoldre.
 * Al final es mostrarà quants s'han resolt, els intents per cadascun
 * @author Ana Gómez Pastor
 */
var prEncertat : Int=0
var noEncertat : Int = 0
var problResolt: Boolean = false
class Problema(num:Int, enunciatProblema: String, exempleInput: String, exempleOutput: String, usuariInput: Int, usuariOutput: String, resolt: Boolean){
    var num: Int = num
    var enunciat: String = enunciatProblema
    var exInput: String = exempleInput
    var exOutput: String = exempleOutput
    var usOutput: String = usuariOutput
    var usInput: Int = usuariInput
    var resolt: Boolean = resolt
}
fun main() {

    println("BENVINGUT AL JUTGE ITB\nIntrtodueix el teu nom")
    val nomUsuari = readln().toString()
    do {
        if(!problResolt){
            exercici1()
        }

        if(!problResolt){
            exercici2()
        }

        if(!problResolt){
            exercici3()
        }
        if(!problResolt){
            exercici4()
        }

        if(!problResolt){
            exercici5()
        }

        estadistiques(nomUsuari, prEncertat, noEncertat)

    }while (prEncertat==5)



}

