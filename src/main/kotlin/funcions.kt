import java.lang.NumberFormatException

/*+
* Funció que conté el primer problema que ha de solucionar l'usuari.
* La funció crida la classe on es troben totes les variables i les definim segons el número del problema,
* l'enunciat, l'exemple, l'output que ha de probar
*/
fun exercici1 (){
    val pr1 = Problema(1,"Text exemple", "5+6", "11", 24, "10+14", false)
    var reintentarPr: String =""
    var respostaProblema: Int =0
    println("${pr1.num}. ${pr1.enunciat}\n${pr1.exInput}\n${pr1.exOutput}")
    println("Vols intentar l'exercici?")
    do{
        val ferExercici: String =
            try {readln()
            } catch (e: NumberFormatException){""
        }
        when(ferExercici){
            "Si" ->{
                println("Intenta resoldre:${pr1.usOutput}\nLa teva resposta:")
                do {
                    respostaProblema = readln().toInt()
                    if (respostaProblema==pr1.usInput){ prEncertat++
                        !pr1.resolt
                        println("Enhorabona, l'has encertat")
                    } else { print("Ups, no es correcte, vols intentar-ho un altre cop?")
                        reintentarPr= readln()
                        if (reintentarPr=="No") noEncertat++
                    }
                }while (respostaProblema!=pr1.usInput || reintentarPr=="Si")
            }
            "No" ->{
                println("Pasem al següent exercici")
                noEncertat++
            }
            else -> println("Opció no válida")
        }
    }while (reintentarPr=="No" && respostaProblema==pr1.usInput)
}

/*+
* Funció que conté el segon problema que ha de solucionar l'usuari.
* La funció crida la classe on es troben totes les variables i les definim segons el número del problema,
* l'enunciat, l'exemple, l'output que ha de probar
*/
fun exercici2(){
    val pr2 = Problema(2,"Text exemple","5+6", "11", 24, "10+14", false)
    var reintentarPr: String =""
    var respostaProblema: Int =0
    println("${pr2.num}. ${pr2.enunciat}\n${pr2.exInput}\n${pr2.exOutput}")
    println("Vols intentar l'exercici?")
    do{
        val ferExercici: String =
            try {readln()
            } catch (e: NumberFormatException){""
            }
        when(ferExercici){
            "Si" ->{
                println("Intenta resoldre:${pr2.usOutput}\nLa teva resposta:")
                do {
                    respostaProblema = readln().toInt()
                    if (respostaProblema==pr2.usInput){ prEncertat++
                        !pr2.resolt
                        println("Enhorabona, l'has encertat")
                    } else { print("Ups, no es correcte, vols intentar-ho un altre cop?")
                        reintentarPr= readln()
                        if (reintentarPr=="No") noEncertat++
                    }
                }while (respostaProblema!=pr2.usInput || reintentarPr=="Si")
            }
            "No" ->{
                println("Pasem al següent exercici")
                noEncertat++
            }
            else -> println("Opció no válida")
        }
    }while (reintentarPr=="No" && respostaProblema==pr2.usInput)
}

/*+
* Funció que conté el tercer problema que ha de solucionar l'usuari.
* La funció crida la classe on es troben totes les variables i les definim segons el número del problema,
* l'enunciat, l'exemple, l'output que ha de probar
*/
fun exercici3(){
    val pr3 = Problema(3,"Text exemple","5+6", "11", 24, "10+14", false)
    var reintentarPr: String =""
    var respostaProblema: Int =0
    println("${pr3.num}. ${pr3.enunciat}\n${pr3.exInput}\n${pr3.exOutput}")
    println("Vols intentar l'exercici?")
    do{
        val ferExercici: String =
            try {readln()
            } catch (e: NumberFormatException){""
            }
        when(ferExercici){
            "Si" ->{
                println("Intenta resoldre:${pr3.usOutput}\nLa teva resposta:")
                do {
                    respostaProblema = readln().toInt()
                    if (respostaProblema==pr3.usInput){ prEncertat++
                        !pr3.resolt
                        println("Enhorabona, l'has encertat")
                    } else { print("Ups, no es correcte, vols intentar-ho un altre cop?")
                        reintentarPr= readln()
                        if (reintentarPr=="No") noEncertat++
                    }
                }while (respostaProblema!=pr3.usInput || reintentarPr=="Si")
            }
            "No" ->{
                println("Pasem al següent exercici")
                noEncertat++
            }
            else -> println("Opció no válida")
        }
    }while (reintentarPr=="No" && respostaProblema==pr3.usInput)
}

/*+
* Funció que conté el quart problema que ha de solucionar l'usuari.
* La funció crida la classe on es troben totes les variables i les definim segons el número del problema,
* l'enunciat, l'exemple, l'output que ha de probar
*/
fun exercici4(){
    val pr4 = Problema(4,"Text exemple","5+6", "11", 24, "10+14", false)
    var reintentarPr: String =""
    var respostaProblema: Int =0
    println("${pr4.num}. ${pr4.enunciat}\n${pr4.exInput}\n${pr4.exOutput}")
    println("Vols intentar l'exercici?")
    do{
        val ferExercici: String =
            try {readln()
            } catch (e: NumberFormatException){""
            }
        when(ferExercici){
            "Si" ->{
                println("Intenta resoldre:${pr4.usOutput}\nLa teva resposta:")
                do {
                    respostaProblema = readln().toInt()
                    if (respostaProblema==pr4.usInput){ prEncertat++
                        !pr4.resolt
                        println("Enhorabona, l'has encertat")
                    } else { print("Ups, no es correcte, vols intentar-ho un altre cop?")
                        reintentarPr= readln()
                        if (reintentarPr=="No") noEncertat++
                    }
                }while (respostaProblema!=pr4.usInput || reintentarPr=="Si")
            }
            "No" ->{
                println("Pasem al següent exercici")
                noEncertat++
            }
            else -> println("Opció no válida")
        }
    }while (reintentarPr=="No" && respostaProblema==pr4.usInput)
}

/*+
* Funció que conté el cinqué problema que ha de solucionar l'usuari.
* La funció crida la classe on es troben totes les variables i les definim segons el número del problema,
* l'enunciat, l'exemple, l'output que ha de probar
*/
fun exercici5(){
    val pr5 = Problema(5,"Text exemple","5+6", "11", 24, "10+14", false)
    var reintentarPr: String =""
    var respostaProblema: Int =0
    println("${pr5.num}. ${pr5.enunciat}\n${pr5.exInput}\n${pr5.exOutput}")
    println("Vols intentar l'exercici?")
    do{
        val ferExercici: String =
            try {readln()
            } catch (e: NumberFormatException){""
            }
        when(ferExercici){
            "Si" ->{
                println("Intenta resoldre:${pr5.usOutput}\nLa teva resposta:")
                do {
                    respostaProblema = readln().toInt()
                    if (respostaProblema==pr5.usInput){ prEncertat++
                        !pr5.resolt
                        println("Enhorabona, l'has encertat")
                    } else { print("Ups, no es correcte, vols intentar-ho un altre cop?")
                        reintentarPr= readln()
                        if (reintentarPr=="No") noEncertat++
                    }
                }while (respostaProblema!=pr5.usInput || reintentarPr=="Si")
            }
            "No" ->{
                println("Pasem al següent exercici")
                noEncertat++
            }
            else -> println("Opció no válida")
        }
    }while (reintentarPr=="No" && respostaProblema==pr5.usInput)
}

fun estadistiques(nomUsuari:String, prEncertat:Int, noEncertat:Int){
    println("""
        _______________________________________
        |RESULTATS:
        |Usuari: $nomUsuari
        |Problemes resolts: $prEncertat
        |Problemes no resolts: $noEncertat
        |Intents en cada problema:
        | ~Problema 1: 
        | ~Problema 2:
        | ~Problema 3:
        | ~Problema 4:
        | ~Problema 5:
        |_______________________________________
        
    """.trimIndent())
}